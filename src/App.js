import React, { Component } from 'react';
import { Switch, Route, Router } from 'react-router-dom'

import Container from './container'
import Album from './pages/album/route'
import wishlist from './pages/white-list/white-list'

class App extends Component {
  render() {
    return (
      <div className="App">
        <Switch>
          <Route exact path='/' component={Container} />
          <Route path='/album/:idAlbum' component={Album} />
          <Route exact path='/wishlist' component={wishlist} />
        </Switch>
      </div>
    );
  }
}

export default App;

