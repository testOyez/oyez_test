import React, { Component } from 'react'

export default class Album extends Component {
  constructor(props) {
    super(props)
    this.state = {
      dataFromServer: [],
      idAlbumLocal: null
    }
  }
  componentDidMount() {
     fetch(`https://jsonplaceholder.typicode.com/albums/${this.props.match.params.idAlbum}`
 )
      .then(response => response.json())
      .then((res) => {
        const arrayOfAlbum = []
        arrayOfAlbum.push(res)
        this.setState({ dataFromServer: arrayOfAlbum, idAlbumLocal: this.props.match.params.idAlbum })
      })
  }
  handAlbum = (id) => {
    this.props.history.push(`/album/${this.state.idAlbumLocal}/photo`);
  }
  renderAlbums = () => {
    const { dataFromServer } = this.state
    const res = dataFromServer.map(elem => {
      return (<div
        onClick={() => this.handAlbum(elem.id)}
        key={elem.id}
        className="album"
        style={{
          'border': '2px solid',
          'padding': '5px',
          'margin': '5px',
          'alignItems': 'center',
          'display': 'flex',
          'cursor': 'pointer'
        }}
      >
        <div>
          <span>album id number : {elem.id}</span>
          <div>album name :{elem.title}</div>
        </div>
      </div >)
    })
    return res
  }
  render() {
    const { dataFromServer } = this.state
    return (
      <div className="container"
        style={{
          'display': 'flex',
          'flexWrap': 'wrap',
          'padding': '50px'
        }}
      >
        {dataFromServer.length > 0 && this.renderAlbums() } </div>
    )
  }
}
