import React, { Component } from 'react'
import { Switch, Route } from 'react-router-dom'

import Photo from '../photo/photo'
import PhotoDescription from '../photo//photo-description'
import Album from './album'

export default class AlbumRoute extends Component {
  render() {
    return (
      <div>
        <Switch>
          <Route exact path='/album/:idAlbum' component={Album} />
          <Route exact path='/album/:idAlbum/photo' component={Photo} />
          <Route exact path='/album/:idAlbum/photo/:idPhoto' component={PhotoDescription} />
        </Switch>
      </div>
    )
  }
}
