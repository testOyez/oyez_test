import { createStore, combineReducers, compose, applyMiddleware } from "redux"
//import query from "react-hoc-query/lib/reducers"
import thunkMiddleware from "redux-thunk"
import createHistory from "history/createBrowserHistory"
import { routerMiddleware } from "react-router-redux"

//import mutation from "modules/mutate/reducers"
import album from "../reducers/album"


const history = createHistory()
let middleware = [
  thunkMiddleware,  
  routerMiddleware(history),
]
const rootReducer = combineReducers({
  album,
})

const store = createStore(
  rootReducer,
  compose(
    applyMiddleware(...middleware),
    window.devToolsExtension ? window.devToolsExtension() : f => f
  )
)

export default store
