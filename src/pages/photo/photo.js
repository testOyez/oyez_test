import React, { Component } from 'react'

export default class Photo extends Component {
  constructor(props) {
    super(props)
    this.state = {
      dataFromServer: []
    }
  }
  componentDidMount() {
    const albumIdLocal = this.props.match.params.idAlbum
     fetch(`https://jsonplaceholder.typicode.com/photos`)
      .then(response => response.json())
      .then((res) => {
        const arrayOfAlbum = res.filter(d => {
          return d.albumId === parseInt(albumIdLocal)
        })
        this.setState({ dataFromServer: arrayOfAlbum })
      })
  }
  handPhoto = (id) => {
    this.props.history.push(`/album/${this.props.match.params.idAlbum}/photo/${id}`);
  }
  renderPhotos = () => {
    const { dataFromServer } = this.state
    const res = dataFromServer.map(elem => {
      return (<div
        onClick={() => this.handPhoto(elem.id)}
        key={elem.id}
        className="photo"
        style={{
          'border': '2px solid',
          'padding': '5px',
          'margin': '5px',
          'alignItems': 'center',
          'display': 'flex',
          'cursor': 'pointer'
        }}
      >
        <div>
          <span>album id number : {this.props.match.params.idAlbum}</span> <br />
          <span>photo id number : {elem.id}</span>
          <div>photo name :{elem.title}</div>
          <div><img width="100" height="100" src={elem.url} alt="test" /></div>
        </div>
      </div >)
    })
    return res
  }
  render() {
    const { dataFromServer } = this.state
    return (
      <div className="container"
        style={{
          'display': 'flex',
          'flexWrap': 'wrap',
          'padding': '50px'
        }}
      >
        {dataFromServer.length > 0 && this.renderPhotos()}
      </div>
    )
  }
}
