const INITIAL_STATE = {
  wishlist: []
}


function album(state = INITIAL_STATE, action) {
  switch (action.type) {
    case "ADD_PHOTO": {
      const res = action.payload
      const arrayOfPhoto = state.wishlist.filter(d => d.id === res.id)
      
      if (arrayOfPhoto.length === 0) {
        state.wishlist.push(res)
      }
      return { ...state }
    }
    default:
      return state
  }
}

export default album
