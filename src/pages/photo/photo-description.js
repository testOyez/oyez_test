import React, { Component } from 'react'
import { connect } from 'react-redux'

import fav from '../../img/coeur.png'

class PhotoDescription extends Component {
  constructor(props) {
    super(props)
    this.state = {
      dataFromServer: ""
    }
  }
  componentDidMount() {
    
    const albumIdLocal = this.props.match.params.idAlbum
     fetch(`https://jsonplaceholder.typicode.com/photos/${this.props.match.params.idPhoto}`)
      .then(response => response.json())
      .then((res) => {
        this.setState({ dataFromServer: res })
      })
  }
  handDescription = (data) => {
    this.props.dispatch({ type: 'ADD_PHOTO', payload: { ...data } })
  }
  renderPhotos = () => {
    const { dataFromServer } = this.state
    
      return (<div
       
        key={dataFromServer.id}
        className="photo"
        style={{
          'border': '2px solid',
          'padding': '5px',
          'margin': '5px',
          'alignItems': 'center',
          'display': 'flex',
          'cursor': 'pointer'
        }}
      >
        <div>
          <span>album id number : {this.props.match.params.idAlbum}</span> <br />
          <span>photo id number : {dataFromServer.id}</span>
          <div>photo name :{dataFromServer.title}</div>
          <div><img width="100" height="100" src={dataFromServer.url} alt="test" /></div>
        </div>
        <button  ><img src={fav} style={{'height':'30px','width':'30px'}} onClick={() => this.handDescription(dataFromServer)}/></button>
      </div >)
    
  
  }
  render() {
    const { dataFromServer } = this.state
    return (
      <div className="container"
        style={{
          'display': 'flex',
          'flexWrap': 'wrap',
          'padding': '50px'
        }}
      >
        {dataFromServer && this.renderPhotos()}
      </div>
    )
  }
}
const mapStateToProps = (state) => {
  return { album: state.album }
}
export default connect(mapStateToProps, null)(PhotoDescription)