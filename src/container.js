import React, { Component } from 'react'

export default class Container extends Component {
  constructor(props) {
    super(props)
    this.state = {
      dataFromServer: []
    }
  }
  componentDidMount() {
     fetch('https://jsonplaceholder.typicode.com/albums')
      .then(response => response.json())
      .then((res) => {
        this.setState({ dataFromServer: res })
      })
  }
  handAlbum = (id) => {
    this.props.history.push(`/album/${id}`);
  }
  renderAlbums = () => {
    const { dataFromServer } = this.state
    const res = dataFromServer.map(elem => {
      return (<div
        onClick={() => this.handAlbum(elem.id)}
        key={elem.id}
        className="album"
        style={{
          'border': '2px solid',
          'padding': '5px',
          'margin': '5px',
          'height': '50px',
          'alignItems': 'center',
          'display': 'flex',
          'cursor': 'pointer'
        }}
      >
        <div>
          <span>album id number : {elem.id}</span>
          <div>album name :{elem.title}</div>
        </div>
      </div >)
    })
    return res
  }
  handlewishlist = () => {
    this.props.history.push('/wishlist')
  }
  render() {
    const { dataFromServer } = this.state
    return (
      <div className="container"
        style={{
          'display': 'flex',
          'flexWrap': 'wrap',
          'padding': '50px'
        }}
      >
        <button onClick={this.handlewishlist}>wishlist Button</button>
        {dataFromServer.length > 0 && this.renderAlbums()}
      </div>
    )
  }
}
