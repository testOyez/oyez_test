import React, { Component } from 'react'
import { connect } from 'react-redux'

class wishlist extends Component {
  constructor(props) {
    super(props)
    this.state = {
      dataFromServer: []
    }
  }
  renderPhotos = () => {
    const { album } = this.props
    const res = album.wishlist.map(elem => {
      return (<div
        key={elem.id}
        className="photo"
        style={{
          'border': '2px solid',
          'padding': '5px',
          'margin': '5px',
          'alignItems': 'center',
          'display': 'flex',
          'cursor': 'pointer'
        }}
      >
        <div>
          <span>photo id number : {elem.id}</span>
          <div>photo name :{elem.title}</div>
          <div><img width="100" height="100" src={elem.url} alt="test" /></div>
        </div>
      </div >)
    })
    return res
  }
  render() {
    return (
      <div className="container"
        style={{
          'display': 'flex',
          'flexWrap': 'wrap',
          'padding': '50px'
        }}
      >
        {this.props.album.wishlist.length > 0 ? this.renderPhotos() : 'list is empty !!'}
      </div>
    )
  }
}
const mapStateToProps = (state) => {
  return { album: state.album }
}
export default connect(mapStateToProps, null)(wishlist)